package routers

import (
	"ginText/gintext/api/goods"

	"github.com/gin-gonic/gin"
)

func InitGoodRouter(router *gin.RouterGroup) {
	goodsRouter := router.Group("Goods")
	{
		goodsRouter.GET("list", goods.List)
		// goodsRouter.Add("add")
	}
}
