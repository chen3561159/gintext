package initialize

import (
	"ginText/gintext/global"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitMysqlDb() {
	dsn := "root:root@tcp(192.168.46.138:3306)/ginText?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	global.Db = db
}
