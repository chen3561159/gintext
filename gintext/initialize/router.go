package initialize

import (
	"ginText/gintext/routers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func InitializeRouter() *gin.Engine {
	engine := gin.Default()
	engine.GET("/health", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"success": true,
		})
	})

	ApiGroup := engine.Group("/v1")
	routers.InitGoodRouter(ApiGroup)

	return engine
}
