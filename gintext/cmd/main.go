package main

import (
	"fmt"
	"ginText/gintext/initialize"
)

func main() {
	initialize.InitMysqlDb()
	engine := initialize.InitializeRouter()
	fmt.Println("hello world1")
	fmt.Println("hello world2")
	fmt.Println("hello world3")
	fmt.Println("hello world4")
	if err := engine.Run("0.0.0.0:8080"); err != nil {
		panic(err)
	}
}
