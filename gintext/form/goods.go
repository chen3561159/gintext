package form

import "github.com/shopspring/decimal"

type Good struct {
	Id    uint
	Name  string
	Price decimal.Decimal
	Color string
	Image string
	Uid   int
}

type User struct {
	Id    uint
	Name  string
	Age   int
	Image string
}
