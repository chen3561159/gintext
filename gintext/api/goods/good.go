package goods

import (
	"fmt"
	"ginText/gintext/form"
	"ginText/gintext/global"
	"net/http"
	"reflect"

	"github.com/gin-gonic/gin"
)

func IdsBykey(key string, val interface{}) []interface{} {
	length := reflect.ValueOf(val).Len()
	var results []interface{}
	for i := 0; i < length; i++ {
		v := reflect.ValueOf(val).Index(i).FieldByName(key)
		switch v.Type().Kind() {
		case reflect.Int:
			results = append(results, v.Int())
		case reflect.String:
			results = append(results, v.String())
		case reflect.Float64:
			results = append(results, v.Float())
		case reflect.Float32:
			results = append(results, v.Float())
		}
	}
	return results
}

func MapByIdToVal(key string, val interface{}) map[uint]interface{} {
	length := reflect.ValueOf(val).Len()
	results := make(map[uint]interface{})
	for i := 0; i < length; i++ {
		v := reflect.ValueOf(val).Index(i).FieldByName(key)
		switch v.Type().Kind() {
		case reflect.Int:
			results[uint(v.Uint())] = reflect.ValueOf(val).Index(i).Interface()
		case reflect.Uint:
			results[uint(v.Uint())] = reflect.ValueOf(val).Index(i).Interface()
		}
	}
	return results
}

func ListToInterfaces() {

}

type ListRes struct {
	form.Good `json:"good"`
	form.User `json:"user"`
}

func List(ctx *gin.Context) {
	var goods []form.Good
	global.Db.Table("gin_goods").Where("uid in ?", []int{1, 2}).Find(&goods)
	res := IdsBykey("Uid", goods)

	var users []form.User
	global.Db.Table("gin_user").Where("id in ?", res).Find(&users)
	maps := MapByIdToVal("Id", users)
	fmt.Println(maps)
	fmt.Println(users)

	responses := make([]ListRes, 0, 10)
	for _, v := range goods {
		responses = append(responses, ListRes{
			Good: v,
			User: form.User{
				Name: maps[uint(v.Uid)].(form.User).Name,
				Age:  maps[uint(v.Uid)].(form.User).Age,
				Id:   maps[uint(v.Uid)].(form.User).Id,
			},
		})
	}
	fmt.Println(responses)
	ctx.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   responses,
		"res":    res,
		"users":  users,
	})
}
